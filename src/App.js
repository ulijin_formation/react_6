import React, { useState, useEffect } from 'react';
function App() {
// Initialisation des états avec useState :
  const [users, setUsers] = useState([]);
  const [newUser, setNewUser] = useState({ username: '', password: '' });
  const [isRegistering, setIsRegistering] = useState(false);
  const [error, setError] = useState(null);
  const [success, setSuccess] = useState(null);

// Utilisation de useEffect pour récupérer les utilisateurs depuis l'API au chargement de la page
  useEffect(() => {
    fetch('http://localhost:3000/users')
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setUsers(data);
      })
      .catch(error => console.error(error));
  }, []);
  
// Fonction appelée lorsque l'utilisateur valide le formulaire de connexion
  const handleLogin = (event) => {
    event.preventDefault();
  
    if ( users.find(user => user.username === newUser.username && user.password === newUser.password)) {
      setError(null);
      setSuccess(`Bonjour ${newUser.username} !`);
    } else {
      console.log(newUser);
      setSuccess(null);
      setError('Identifiant ou mot de passe incorrect');
    }
  }

 // Fonction appelée lorsque l'utilisateur valide le formulaire d'inscription
 const handleRegister = (event) => {
  event.preventDefault();
  const matchingUser = users.find(user => user.username === newUser.username);
  if (typeof matchingUser !== 'undefined') {
    setError('Cet identifiant est déjà utilisé');
    setSuccess(null);
  } else {
    setError(null);
    fetch('http://localhost:3000/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newUser)
    })
      .then(response => response.json())
      .then(data => {
        setUsers([...users, { id: data.id, ...newUser }]);
        setSuccess(`Bienvenue ${newUser.username} !`);
      })
      .catch(error => console.error(error));
    setNewUser({ username: '', password: '' });
  }
}
  // Fonction appelée lorsque l'utilisateur saisit des informations dans les champs du formulaire
  const handleUserChange = (event) => {
    setNewUser({ ...newUser, [event.target.name]: event.target.value });
  }

  // Fonction appelée lorsque l'utilisateur clique sur le bouton pour passer d'un mode a l'autre (connexion/inscription)
  const handleSwitchMode = () => {
    setNewUser({ username: '', password: '' });
    setError(null);
    setSuccess(null);
    setIsRegistering(!isRegistering);
  }
  
  // Affichage de la page
  return (
    <div className='App'>
      <h1>{isRegistering ? 'Inscription' : 'Connexion'}</h1>
      {error && <p className="error">{error}</p>}
      {success && <p className="success">{success}</p>}

      <form onSubmit={isRegistering ? handleRegister : handleLogin}>
        <label htmlFor="username">Nom d'utilisateur </label>
        <input type="text" id="username" name="username" value={newUser.username} onChange={handleUserChange} />
        <label htmlFor="password">Mot de passe </label>
        <input type="password" id="password" name="password" value={newUser.password} onChange={handleUserChange} />

        <div className='btn-box'>
          <div className='btn-box-login'>
            <button className='btn-login' type="submit">{isRegistering ? 'Inscription' : 'Connexion'}</button>
          </div>
          <div className='btn-box-register'>
            <button className='btn-register' onClick={handleSwitchMode}>{isRegistering ? 'Connexion' : 'Inscription'}</button>
          </div>
        </div>
      </form>

    </div>
  );
}
export default App;