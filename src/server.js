const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3000;

// Autorisation du traitement des données envoyées en format JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//Cors
app.use(cors({
    origin: function(origin, callback){
        console.log('ORIGIN ', origin)
        if(!origin) return callback(null, true);
        return callback(null, true);
      }
    }));

const db = new sqlite3.Database('database.db');



// Route pour obtenir la liste de tous les utilisateurs
app.get('/users', (req, res) => {
//Requête pour obtenir tous les utilisateurs
  db.all('SELECT * FROM users', (err, rows) => {
    if (err) {
      console.error(err);
      res.status(500).send('Erreur serveur');
    } else {
      res.json(rows);
    }
  });
});
// Route pour ajouter un nouvel utilisateur
app.post('/users', (req, res) => {
  const { username, password } = req.body;
  if (!username || !password) {
// Vérifie que toutes les données requises sont présentes
    res.status(400).send('Erreur de requête');
    return;
  }
 //Requête pour ajouter un utilisateur
  db.run('INSERT INTO users (username, password) VALUES (?, ?)', [username, password], function(err) {
    if (err) {
      console.error(err);
      res.status(500).send('Erreur serveur');
    } else {
      // Renvoie l'ID de l'utilisateur ajouté en JSON
      res.json({ id: this.lastID });
    }
  });
});

app.listen(port, () => {
  console.log(`Connexion sur le port http://localhost:${port}`);
});